<?php

namespace App\Controller;

use App\Service\StarWarsApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private const PAGE_SIZE = 10;

    public function __construct(private readonly StarWarsApi $starWarsApi) {}

    #[Route('/', name: 'app_home')]
    public function index(Request $request): Response
    {
        $page = $request->query->getInt('page', 1);

        $allPersonnages = $this->starWarsApi->getPersonnages();
        $pagination = $this->paginate($allPersonnages, $page);

        return $this->render('home/index.html.twig', [
            'personnages' => $pagination['results'],
            'pagination' => $pagination,
        ]);
    }

    private function paginate(array $items, int $page): array
    {
        $offset = ($page - 1) * self::PAGE_SIZE;
        $paginatedItems = array_slice($items, $offset, self::PAGE_SIZE);

        return [
            'results' => $paginatedItems,
            'total' => count($items),
            'page' => $page,
            'pageSize' => self::PAGE_SIZE,
        ];
    }

    #[Route('/personnage/{id}', name: 'app_personnage')]
    public function personnage(int $id): Response
    {
        return $this->render('home/personnage.html.twig', [
            'personnage' => $this->starWarsApi->getPersonnage($id),
        ]);
    }
}

