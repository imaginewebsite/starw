<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class StarWarsApi
{
    private const BASE_URL = 'https://swapi.py4e.com/api/people/';


    public function __construct(private readonly HttpClientInterface $httpClient) {
    }

    public function getPersonnages(): array
    {
        return $this->makePagedRequest();
    }

    public function getPersonnage(int $id): array
    {
        return $this->makeRequest($id);
    }

    private function makeRequest(?int $id = null): array
    {
        $url = $id ? self::BASE_URL . $id : self::BASE_URL;

        return $this->fetchData($url);
    }

    private function makePagedRequest(): array
    {
        $persos = [];
        $page = 1;

        do {
            $url = self::BASE_URL . '?page=' . $page;

            $data = $this->fetchData($url);
            $persos = array_merge($persos, $data['results']);
            $page++;

        } while ($data['next'] !== null);

        return $persos;
    }

    private function fetchData(string $url): array
    {
        $response = $this->httpClient->request('GET', $url);

        return $response->toArray();
    }
}